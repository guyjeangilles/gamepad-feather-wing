#include <Adafruit_NeoPixel.h>

#define LEFT_TRIGGER A4
#define RIGHT_TRIGGER A2

void setup() {
  Serial.begin(115200);
}

void loop() {
  Serial.print("Left Trigger: ");
  Serial.print(analogRead(LEFT_TRIGGER));
  Serial.print(" Right Trigger: ");
  Serial.println(analogRead(RIGHT_TRIGGER));
}
