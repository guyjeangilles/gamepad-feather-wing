#include "Adafruit_TinyUSB.h" // to use Arduino's Serial class

#define NUM_PISO_CHIPS 3 // how many shift register chips are daisy-chained.
#define PISO_DATA_WIDTH NUM_PISO_CHIPS * 8 // width of data (how many ext lines).
#define PISO_PULSE_WIDTH_USEC 5 //width of pulse to trigger the shift register to read and latch.
#define PISO_POLL_DELAY_MSEC 5 //optional delay between shift register reads.
#define PISO_OUT_DATA_TYPE uint32_t //You will need to change the "int" to "long" if more than two chips
#define SHIFT_PIN 6 // SH/~LD shift or load input on the SN74HC165N
#define CLK_INH_PIN 9 // clock inhibit pin
#define SER_OUT_PIN 10 // Qh or serial output
#define CLK_PIN 11

// shift register value for each button
#define RESET_VAL 1024
#define HAT_UP_VAL 1
#define HAT_RIGHT_VAL 4
#define HAT_DOWN_VAL 8
#define HAT_LEFT_VAL 2
#define BUTTON_0_VAL 524288
#define BUTTON_1_VAL 262144
#define BUTTON_2_VAL 65536
#define BUTTON_3_VAL 131072
#define BUTTON_4_VAL 32
#define BUTTON_5_VAL 4194304
#define BUTTON_6_VAL 64
#define BUTTON_7_VAL 2097152
#define BUTTON_8_VAL 128
#define BUTTON_9_VAL 16
#define BUTTON_10_VAL 8388608
#define BUTTON_11_VAL 1048576
#define BUTTON_12_VAL 256
#define BUTTON_13_VAL 512
#define BUTTON_14_VAL 2048
#define BUTTON_15_VAL 4096

PISO_OUT_DATA_TYPE read_PISOs(void);

void setup() {
  Serial.begin(115200);
  pinMode(SHIFT_PIN, OUTPUT);
  pinMode(CLK_INH_PIN, OUTPUT);
  pinMode(CLK_PIN, OUTPUT);
  pinMode(SER_OUT_PIN, INPUT);
  digitalWrite(CLK_PIN, LOW);
  digitalWrite(SHIFT_PIN, HIGH);

  while (!Serial){
    // wait until Serial is ready
  }
  Serial.println("SN74HC165N PISO shift register test...");
}

void loop() {
  PISO_OUT_DATA_TYPE piso_bits = read_PISOs();

  if(piso_bits & RESET_VAL){
    Serial.print("Reset, ");
  }
  if(piso_bits & HAT_UP_VAL){
    Serial.print("Up, ");
  }
  if (piso_bits & HAT_RIGHT_VAL){
    Serial.print("Right, ");
  }
  if (piso_bits & HAT_DOWN_VAL){
    Serial.print("Down, ");
  }
  if (piso_bits & HAT_LEFT_VAL){
    Serial.print("Left, ");
  }
  if (piso_bits & BUTTON_0_VAL){
    Serial.print("Button 0, ");
  }
  if (piso_bits & BUTTON_1_VAL){
    Serial.print("Button 1, ");
  }
  if (piso_bits & BUTTON_2_VAL){
    Serial.print("Button 2, ");
  }
  if (piso_bits & BUTTON_3_VAL){
    Serial.print("Button 3, ");
  }
  if (piso_bits & BUTTON_4_VAL){
    Serial.print("Button 4, ");
  }
  if (piso_bits & BUTTON_5_VAL){
    Serial.print("Button 5, ");
  }
  if (piso_bits & BUTTON_6_VAL){
    Serial.print("Button 6, ");
  }
  if (piso_bits & BUTTON_7_VAL){
    Serial.print("Button 7, ");
  }
  if (piso_bits & BUTTON_8_VAL){
    Serial.print("Button 8, ");
  }
  if (piso_bits & BUTTON_9_VAL){
    Serial.print("Button 9, ");
  }
  if (piso_bits & BUTTON_10_VAL){
    Serial.print("Button 10, ");
  }
  if (piso_bits & BUTTON_11_VAL){
    Serial.print("Button 11, ");
  }
  if (piso_bits & BUTTON_12_VAL){
    Serial.print("Button 12, ");
  }
  if (piso_bits & BUTTON_13_VAL){
    Serial.print("Button 13, ");
  }
  if (piso_bits & BUTTON_14_VAL){
    Serial.print("Button 14, ");
  }
  if (piso_bits & BUTTON_15_VAL){
    Serial.print("Button 15, ");
  }

  Serial.println("");
}

PISO_OUT_DATA_TYPE read_PISOs(void)
{
  long bitVal;
  PISO_OUT_DATA_TYPE bytesVal = 0;

  // latch data lines
  digitalWrite(CLK_INH_PIN, HIGH);
  digitalWrite(SHIFT_PIN, LOW);
  delayMicroseconds(PISO_PULSE_WIDTH_USEC);
  digitalWrite(SHIFT_PIN, HIGH);
  digitalWrite(CLK_INH_PIN, LOW);

  //read each bit value from the serial out line of the SN74HC165N.
  for (int i = 0; i < PISO_DATA_WIDTH; i++)
  {
    bitVal = digitalRead(SER_OUT_PIN);

    // Set the corresponding bit in bytesVal.
    bytesVal |= (bitVal << ((PISO_DATA_WIDTH - 1) - i));

    // Pulse the Clock (rising edge shifts the next bit).
    digitalWrite(CLK_PIN, HIGH);
    delayMicroseconds(PISO_PULSE_WIDTH_USEC);
    // delayMicroseconds(PISO_POLL_DELAY_MSEC);
    digitalWrite(CLK_PIN, LOW);
  }

  return bytesVal;
}
