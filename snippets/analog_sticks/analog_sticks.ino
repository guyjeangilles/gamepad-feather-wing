#include "Adafruit_TinyUSB.h"
#define RESET_PIN 13

#define ANALOG_STICK_0_X A5
#define ANALOG_STICK_0_Y A3
#define ANALOG_STICK_1_X A0
#define ANALOG_STICK_1_Y A1

void setup() {
  pinMode(RESET_PIN, OUTPUT);
  digitalWrite(RESET_PIN, HIGH);

  Serial.begin(115200);
}

void loop() {
  Serial.print("Analog Stick 0 X-axis: ");
  Serial.println(map(1024 - analogRead(ANALOG_STICK_0_X), 0, 1024, -127, 127));
  Serial.print("Analog Stick 0: Y-axis: ");
  Serial.println(map(analogRead(ANALOG_STICK_0_Y), 0, 1024, -127, 127));
  Serial.print("Analog Stick 1: X-axis: ");
  Serial.println(map(analogRead(ANALOG_STICK_1_X), 0, 1024, -127, 127));
  Serial.print("Analog Stick 1: Y-axis: ");
  Serial.println(map(1024 - analogRead(ANALOG_STICK_1_Y), 0, 1024, -127, 127));
  Serial.println("");
  delay(10);
}
