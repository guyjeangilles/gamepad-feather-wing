# Gamepad Feather Wing

## Table of Contents
- [Introduction](#introduction)
- [Layout](#layout)
- [How To Use](#how-to)
- [Known Issues](#known-issues)

<a name="introduction"></a>
## Introduction
Turn any USB and/or BLE compatible [feather board](https://learn.adafruit.com/adafruit-feather) into a gamepad with _truly analog triggers_ using this feather wing. It includes 2 analog sticks, 2 analog triggers, 4 directional pads, and 15 additional digital buttons.

Schematic and Bill of Materials for the PCB are in the KiCad folder. The KiCad project uses the [Sparkfun KiCad Libraries](https://github.com/sparkfun/SparkFun-KiCad-Libraries), which is a submodule of this repo.

![Bare PCB](media/bare_board.jpg)
![Assembled PCB Front](media/assembled_pcb_front.jpg)
![Assembled PCB Back](media/assembled_pcb_back.jpg)
![Exterior Analog Trigger](media/analog_trigger_exterior.jpg)
![Interior Analog Trigger](media/analog_trigger_interior.jpg)

<a name="layout"></a>
## Layout
```
          ____________________________              __
         / [__RX__]          [__RY__] \               |
        / [__ 4  __]        [__ 5  __] \              | Front Triggers
     __/________________________________\__         __|
    /  ___                           ___   \          |           
   /  /   \                         /   \   \         |
  /  |  9  |                       | 10  |   \        | Analog Sticks
 /    \___/                         \___/     \       |
/               __           __        _       \    __|
|         /\   |6 |          |7 |     (0)      |      |
\         ||      __        __                 /      |
 \    <===DP===> |8 |      |11|   (3) -|- (1) /       |
 /\       ||                                 /\       | Face Buttons
/  \      \/                          (2)   /  \      |
|   \        |12|  |13|  |14|  |15|        /   |      |
|    \____________________________|RESET|_/    |    __|
|        /                            \        |     
|       /                              \       |
 \_____/                                \_____/
```

<a name="how-to"></a>
## How To Use
If a USB cable is connected to the feather wing when powered on, the software will default to a USB gamepad. If a USB connection can't be established, the software will start advertising as a BLE gamepad. Pair as usual with a host device once advertising. No drivers needed for either the USB or BLE gamepad configuration. Go and play your game!

To switch from a USB to a BLE connection, hold the reset button at the bottom right of the feather wing, remove the USB cable, then release the reset button. Once the software starts advertising as a BLE device, plugging in the USB cable will charge the battery without switching to a USB HID connection.

Similarly, to switch from a BLE connection to USB, hold the reset button, plug in a USB cable, then release the reset button.

NOTE: There is no physical off button or switch.

<a name="known-issues"></a>
## Known Issues
- Back annular rings of analog stick footprint are too small
- The direction of the analog trigger potentiometers are flipped, but this has been corrected in software
- Analog stick digital button numbers are in a weird order
