#include <Arduino.h>

#define NUM_PISO_CHIPS 3 // how many shift register chips are daisy-chained.
#define PISO_DATA_WIDTH NUM_PISO_CHIPS * 8 // width of data (how many ext lines).
#define PISO_PULSE_WIDTH_USEC 5 //width of pulse to trigger the shift register to read and latch.
#define PISO_POLL_DELAY_MSEC 5 //optional delay between shift register reads.
#define PISO_OUT_DATA_TYPE uint32_t //You will need to change the "int" to "long" if more than two chips
#define SHIFT_PIN 6 // SH/~LD shift or load input on the SN74HC165N
#define CLK_INH_PIN 9 // clock inhibit pin
#define SER_OUT_PIN 10 // Qh or serial output
#define CLK_PIN 11

#define RESET_MASK 1024
#define HAT_UP_MASK 1
#define HAT_RIGHT_MASK 4
#define HAT_DOWN_MASK 8
#define HAT_LEFT_MASK 2
#define BUTTON_0_MASK 524288
#define BUTTON_1_MASK 262144
#define BUTTON_2_MASK 65536
#define BUTTON_3_MASK 131072
#define BUTTON_4_MASK 32
#define BUTTON_5_MASK 4194304
#define BUTTON_6_MASK 64
#define BUTTON_7_MASK 2097152
#define BUTTON_8_MASK 128
#define BUTTON_9_MASK 16
#define BUTTON_10_MASK 8388608
#define BUTTON_11_MASK 1048576
#define BUTTON_12_MASK 256
#define BUTTON_13_MASK 512
#define BUTTON_14_MASK 2048
#define BUTTON_15_MASK 4096

PISO_OUT_DATA_TYPE read_PISOs(void)
{
  long bitVal;
  PISO_OUT_DATA_TYPE bytesVal = 0;

  // latch data lines
  digitalWrite(CLK_INH_PIN, HIGH);
  digitalWrite(SHIFT_PIN, LOW);
  delayMicroseconds(PISO_PULSE_WIDTH_USEC);
  digitalWrite(SHIFT_PIN, HIGH);
  digitalWrite(CLK_INH_PIN, LOW);

  //read each bit value from the serial out line of the SN74HC165N.
  for (int i = 0; i < PISO_DATA_WIDTH; i++)
  {
    bitVal = digitalRead(SER_OUT_PIN);

    // Set the corresponding bit in bytesVal.
    bytesVal |= (bitVal << ((PISO_DATA_WIDTH - 1) - i));

    // Pulse the Clock (rising edge shifts the next bit).
    digitalWrite(CLK_PIN, HIGH);
    delayMicroseconds(PISO_PULSE_WIDTH_USEC);
    digitalWrite(CLK_PIN, LOW);
  }

  return bytesVal;
}

void setup_PISOs(){
  pinMode(SHIFT_PIN, OUTPUT);
  pinMode(CLK_INH_PIN, OUTPUT);
  pinMode(CLK_PIN, OUTPUT);
  pinMode(SER_OUT_PIN, INPUT);
  digitalWrite(CLK_PIN, LOW);
  digitalWrite(SHIFT_PIN, HIGH);
}

// converts shift register data into gamepad hat codes
uint8_t get_hat(){
  uint8_t hat = 0;
  hat |= (read_PISOs() & (HAT_UP_MASK + HAT_RIGHT_MASK + HAT_DOWN_MASK + HAT_LEFT_MASK));

  switch (hat){
    case 1: // UP hat
      return 1;
    case 5: // UP and RIGHT hat
      return 2;
    case 4: // RIGHT hat
      return 3;
    case 12: // DOWN and RIGHT hat
      return 4;
    case 8: // DOWN hat
      return 5;
    case 10: // DOWN and LEFT hat
      return 6;
    case 2: // LEFT hat
      return 7;
    case 3: // UP and LEFT hat
      return 8;
    default:
      return 0;
  }
}

uint32_t get_buttons(){
  uint32_t buttons;
  PISO_OUT_DATA_TYPE PISO_state = read_PISOs();
  PISO_OUT_DATA_TYPE button_masks[16] = {
    BUTTON_0_MASK,
    BUTTON_1_MASK,
    BUTTON_2_MASK,
    BUTTON_3_MASK,
    BUTTON_4_MASK,
    BUTTON_5_MASK,
    BUTTON_6_MASK,
    BUTTON_7_MASK,
    BUTTON_8_MASK,
    BUTTON_9_MASK,
    BUTTON_10_MASK,
    BUTTON_11_MASK,
    BUTTON_12_MASK,
    BUTTON_13_MASK,
    BUTTON_14_MASK,
    BUTTON_15_MASK,
  };

  // set or clear gamepad buttons based off shift register state
  for (uint8_t i = 0; i < 16; i++){
    if (PISO_state & button_masks[i]){
      buttons |= (1UL << i);
    } else {
      buttons &= ~(1UL << i);
    }
  }

  return buttons;
}

bool check_reset(void){
  return read_PISOs() & RESET_MASK;
}
