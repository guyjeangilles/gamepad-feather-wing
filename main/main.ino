#include <bluefruit.h>
#include "Adafruit_TinyUSB.h"
#include "analog_axes.h"
#include "buttons.h"

#define RESET_PIN 13

BLEDis bledis;
BLEHidGamepad blegamepad;

Adafruit_USBD_HID usb_hid;

uint8_t const desc_hid_report[] =
{
  TUD_HID_REPORT_DESC_GAMEPAD()
};

hid_gamepad_report_t gp;
bool use_usb_flag = 1;
void start_adv(void);
void send_hid_report(void);
bool is_connected(void);
void setup_hid(void);

void setup()
{
  // setup reset
  pinMode(RESET_PIN, OUTPUT);
  digitalWrite(RESET_PIN, HIGH);

  Serial.begin(115200); // setup Serial
  setup_PISOs(); // intialize buttons
  setup_hid();
}

void loop()
{
  // reset if user requests it
  if (check_reset()){
    Serial.println("Reset");
    digitalWrite(RESET_PIN, LOW);
  }

  // nothing to do if not connected
  if (!is_connected()) return;

  gp.hat = get_hat();
  gp.buttons = get_buttons();
  gp.x = get_left_x_axis();
  gp.y = get_left_y_axis();
  gp.z = get_right_x_axis();
  gp.rz = get_right_y_axis();
  gp.rx = get_left_trigger();
  gp.ry = get_right_trigger();
  send_hid_report();
}

void start_adv(void)
{
  // Advertising packet
  Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
  Bluefruit.Advertising.addTxPower();
  Bluefruit.Advertising.addAppearance(BLE_APPEARANCE_HID_GAMEPAD);

  // Include BLE HID service
  Bluefruit.Advertising.addService(blegamepad);

  // There is enough room for the dev name in the advertising packet
  Bluefruit.Advertising.addName();

  /* Start Advertising
   * - Enable auto advertising if disconnected
   * - Interval:  fast mode = 20 ms, slow mode = 152.5 ms
   * - Timeout for fast mode is 30 seconds
   * - Start(timeout) with timeout = 0 will advertise forever (until connected)
   *
   * For recommended advertising interval
   * https://developer.apple.com/library/content/qa/qa1931/_index.html
   */
  Bluefruit.Advertising.restartOnDisconnect(true);
  Bluefruit.Advertising.setInterval(32, 244);    // in unit of 0.625 ms
  Bluefruit.Advertising.setFastTimeout(30);      // number of seconds in fast mode
  Bluefruit.Advertising.start(0);                // 0 = Don't stop advertising after n seconds
}

void send_hid_report(){
  if (use_usb_flag){
    usb_hid.sendReport(0, &gp, sizeof(gp));
  } else {
    blegamepad.report(&gp);
  }
}

void setup_hid(){
  usb_hid.setPollInterval(2);
  usb_hid.setReportDescriptor(desc_hid_report, sizeof(desc_hid_report));
  usb_hid.begin();

  // wait until device mounted
  unsigned long mount_start = millis();
  while( !TinyUSBDevice.mounted() ){
    if ((millis() - mount_start) > 5000){
      use_usb_flag = 0;
      break;
    }
    delay(1);
  }

  // configure BLE HID and start advertising
  if (!use_usb_flag){
    Bluefruit.begin();
    Bluefruit.setTxPower(4);
    bledis.setManufacturer("Adafruit Industries");
    bledis.setModel("Gamepad Feather Wing");
    bledis.begin();
    blegamepad.begin();
    start_adv();
  }
}

bool is_connected(){
  if (use_usb_flag){
    return usb_hid.ready();
  }
  return Bluefruit.connected();
}
