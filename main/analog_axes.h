int8_t get_left_x_axis(void);
int8_t get_left_y_axis(void);
int8_t get_right_x_axis(void);
int8_t get_right_y_axis(void);
int8_t get_left_trigger(void);
int8_t get_right_trigger(void);
