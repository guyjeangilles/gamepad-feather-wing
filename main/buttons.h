void setup_PISOs(void);
uint8_t get_hat(void); // converts shift register data into gamepad hat codes
uint32_t get_buttons(void);
bool check_reset(void);
