#include <Arduino.h>
#include "analog_axes.h"

#define LEFT_STICK_X A5
#define LEFT_STICK_Y A3
#define RIGHT_STICK_X A0
#define RIGHT_STICK_Y A1
#define LEFT_TRIGGER A4
#define RIGHT_TRIGGER A2

#define LEFT_TRIGGER_UP_VAL 555
#define LEFT_TRIGGER_DOWN_VAL 369
#define RIGHT_TRIGGER_UP_VAL 510
#define RIGHT_TRIGGER_DOWN_VAL 345


int8_t get_left_x_axis(){
  return int8_t(map(1024 - analogRead(LEFT_STICK_X), 0, 1024, -127, 127));
}

int8_t get_left_y_axis(){
  return int8_t(map(1024 - analogRead(LEFT_STICK_Y), 0, 1024, -127, 127));
}

int8_t get_right_x_axis(){
  return int8_t(map(analogRead(RIGHT_STICK_X), 0, 1024, -127, 127));
}

int8_t get_right_y_axis(){
  return int8_t(map(1024 - analogRead(RIGHT_STICK_Y), 0, 1024, -127, 127));
}

int8_t get_left_trigger(){
  return int8_t(map(analogRead(LEFT_TRIGGER), LEFT_TRIGGER_DOWN_VAL, LEFT_TRIGGER_UP_VAL, -127, 127));
}

int8_t get_right_trigger(){
  return int8_t(map(analogRead(RIGHT_TRIGGER), RIGHT_TRIGGER_DOWN_VAL, RIGHT_TRIGGER_UP_VAL, -127, 127));
}
